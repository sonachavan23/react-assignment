import {
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { Login, Admin, UserDetails, Products, Cart, Header } from './modules';
import * as Constants from './Routes';
import { ProtectedAdminRoutes, ProtectedUsersRoutes } from "./configRoutes/index";

function App() {
  return (
    <div className="App">
      <div className="container-fluid">
        <div className="">
          <Switch>
            <Route exact path='/' component={Login} />
            <Route path={Constants.Login} component={Login} />
            <Header>
              <ProtectedAdminRoutes path={Constants.Admin} component={Admin} />
              <ProtectedAdminRoutes path={Constants.UserDetails} component={UserDetails} />
              <ProtectedUsersRoutes path={Constants.Products} component={Products} />
              <ProtectedUsersRoutes path={Constants.Cart} component={Cart} />
            </Header>

            <Redirect from="*" to="/" />
          </Switch>
        </div>
      </div>
    </div>
  );
}

export default App;
