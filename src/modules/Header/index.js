import { Fragment, React } from 'react';
import { withRouter } from 'react-router-dom'

function header(props) {

    const logout = () => {
        localStorage.clear();
        props.history.push('/login');
    }

    return (
        <Fragment>
            {
                <div className="float-right mr-5">
                    <h3 className='text-danger'>
                        <i className="fa fa-sign-out" onClick={() => logout()}></i>
                    </h3>
                </div>
            }
            {props.children}
        </Fragment>

    )
}

export default withRouter(header);
