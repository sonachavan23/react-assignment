﻿import * as ActionType from '../../ActionTypes';
const initialState = {
	data: [],
	categories: [],
};


export default function productState(state = initialState, action) {
	switch (action.type) {
		case ActionType.PRODUCT_SUCCESS:
			return {
				...state,
				data: action.payload
			}

		case ActionType.CATEGORY_SUCCESS:
			return {
				...state,
				categories: action.payload
			}
		default:
			return state
	}
}